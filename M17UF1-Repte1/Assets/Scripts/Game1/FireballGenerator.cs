using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class FireballGenerator : MonoBehaviour
{

    //private GameObject _tiles;
    private int _minPosition = -7;
    private int _maxPosition = 7;
    private int _yPosition = 6;
    private float _position;
    public GameObject FireBalPrefab;

    [SerializeField]
    private float _timer;
    private float _time = 0.5f;

    private float _timer2;
    private float _timeWin = 10.0f;

    // Start is called before the first frame update
    void Start()
    {
        _timer = 0;
        _timer2 = 0;
    }

    // Update is called once per frame
    void Update()
    {
        _timer += Time.deltaTime;
        _timer2 += Time.deltaTime;
        if (_timer > _time) GenerateFireBall();
        if (_timer2 > _timeWin) SceneManager.LoadScene("GameOverScreen");
    }

    void GenerateFireBall()
    {
        _timer = 0;
        _position = Random.Range(_minPosition, _maxPosition + 1);
        Instantiate(FireBalPrefab, new Vector3(_position, _yPosition, 0), new Quaternion(0, 0, 0, 0));
        GameObject.Find("GameManager").GetComponent<GameManager>().FireBalls++;
    }
}

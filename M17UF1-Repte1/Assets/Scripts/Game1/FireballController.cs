using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using static UnityEditor.Experimental.AssetDatabaseExperimental.AssetDatabaseCounters;

public class FireballController : MonoBehaviour
{
    public Sprite[] Sprites;
    private int _indexSprite;
    private int _frameRate = 12;
    private int _count;

    

    // Start is called before the first frame update
    void Start()
    {
        _indexSprite = 0;
        _count = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Animation();
        if (this.gameObject.transform.position.y <= -10) GameObject.Destroy(this.gameObject);
    }

    void Animation()
    {
        if (_count >= _frameRate)
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = Sprites[_indexSprite];
            _indexSprite++;
            if (_indexSprite > Sprites.Length - 1) _indexSprite = 0;
            _count = 0;
        }
        _count++;
    }

    
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject.Find("Text_Fireballs").GetComponent<Text>().text = GameObject.Find("GameManager").GetComponent<GameManager>().FireBalls.ToString() + " boles de foc generades";

        if (GameObject.Find("GameManager").GetComponent<GameManager>().Player.GetComponent<PlayerData>().PlayerLife > 0 && GameObject.Find("GameManager").GetComponent<GameManager>().gameType == GameType.Game1)
            GameObject.Find("Result").GetComponent<Text>().text = GameObject.Find("GameManager").GetComponent<GameManager>().Player.GetComponent<PlayerData>().PlayerName + " ha guanyat!";
        else if (GameObject.Find("GameManager").GetComponent<GameManager>().gameType == GameType.Game1)
            GameObject.Find("Result").GetComponent<Text>().text = GameObject.Find("GameManager").GetComponent<GameManager>().Player.GetComponent<PlayerData>().PlayerName + " ha perdut.";
        else if (GameObject.Find("GameManager").GetComponent<GameManager>().gameType == GameType.Game2)
            GameObject.Find("Result").GetComponent<Text>().text = GameObject.Find("GameManager").GetComponent<GameManager>().Player.GetComponent<PlayerData>().PlayerName + " ha guanyat!";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;
using UnityEngine;


public enum GameType
{
    None,
    Game1,
    Game2
}
public class GameManager : MonoBehaviour
{
    
    public GameType gameType;
    public int FireBalls;
    public GameObject Player;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        FireBalls = 0;
        Player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        switch (this.gameObject.GetComponent<GameManager>().gameType)
        {
            case GameType.None:
                Player.SetActive(false);
                break;
            case GameType.Game1:
                Player.SetActive(true);
                break;
            case GameType.Game2:
                Player.SetActive(false);
                break;
        }


    }
}

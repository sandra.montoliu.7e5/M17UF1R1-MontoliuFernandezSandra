using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{

    // Carregar una nova escena
    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void Replay()
    {
        if (GameObject.Find("GameManager").GetComponent<GameManager>().gameType == GameType.Game1)
        {
            Game1();
            LoadScene("GameScene1");
        }
        else if (GameObject.Find("GameManager").GetComponent<GameManager>().gameType == GameType.Game2)
        {
            Game2();
            LoadScene("GameScene2");
        }
        else
            LoadScene("StartScreen");
    }

    public void LoadMenu()
    {
        LoadScene("StartScreen");
    }

    public void LoadGame1()
    {
        Game1();
        LoadScene("GameScene1");
    }

    public void LoadGame2()
    {
        Game2();
        LoadScene("GameScene2");
    }

    public void Game1()
    {
        GameObject.Find("GameManager").GetComponent<GameManager>().Player.GetComponent<Transform>().position = new Vector3(0f, 0f, 0f); //ChangePositionCharacter
        GameObject.Find("GameManager").GetComponent<GameManager>().gameType = GameType.Game1;
    }

    public void Game2()
    {
        GameObject.Find("GameManager").GetComponent<GameManager>().Player.GetComponent<Transform>().position = new Vector3(0f, 0f, 0f); //ChangePositionCharacter
        GameObject.Find("GameManager").GetComponent<GameManager>().gameType = GameType.Game2;
    }

    
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using UnityEngine.SceneManagement;

public class PlayerData : MonoBehaviour
{
    public string PlayerName;
    private float _positionMultiplier = 0.003f;
    private Rigidbody2D rigidbody2D;
    private float _jumpSpeed = 4f;
    //      private Ray2D _rayJump;
    //      private float _rangeRayJump = 5;
    //      private Vector3 _directionRayJump;

    private RaycastHit2D _raycastHit2D;
    public float _raycastDistance;
    [SerializeField]
    public float PlayerLife;


    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        // No destruir l'objecte al iniciar nova escena.
        DontDestroyOnLoad(this.gameObject);
        rigidbody2D = transform.GetComponent<Rigidbody2D>();

        PlayerLife = 1.0f;
        
    }


    // Update is called once per frame
    void Update()
    {
        ManualControlPlayer();
        
        ControlLife();
        if (this.gameObject.transform.position.y <= -10 && SceneManager.GetActiveScene().name == "GameScene1") PlayerLife = 0;
    }

    public void RenameCharacterStart()
    {
        this.GetComponent<PlayerData>().PlayerName = GameObject.Find("InputField_CharacterName").GetComponent<InputField>().text; //ChangeNameCharacter
    }

    void ManualControlPlayer()
    {
        // Change Horitzontal Axis
        transform.position = new Vector3(_positionMultiplier * Input.GetAxis("Horizontal") + transform.position.x, transform.position.y, transform.position.z);

        // Jump
        //      _directionRayJump = Vector3.forward;
        //      _rayJump = new Ray2D(transform.position, transform.TransformDirection(_directionRayJump * _rangeRayJump));
        //      Debug.DrawRay(transform.position, transform.TransformDirection(_directionRayJump * _rangeRayJump));
        //      RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.TransformDirection(Vector3.up, 10f));
        
        _raycastHit2D = Physics2D.Raycast(new Vector3(transform.position.x, transform.position.y - 0.8f), Vector2.down, _raycastDistance);
        
        if (_raycastHit2D.collider != null) // Si el raig xoca amb algo
        {
            Debug.DrawRay(new Vector3(transform.position.x,transform.position.y), Vector2.down, Color.green);
            if (Input.GetAxis("Vertical") > 0)
            {
                rigidbody2D.velocity = Vector2.up * _jumpSpeed;
            }
        }
        if (_raycastHit2D.collider == null) // El raig NO xoca amb res
        {
            Debug.DrawRay(new Vector3(transform.position.x, transform.position.y), Vector2.down, Color.red);
        }
        
        


        // Girar el Sprite en el moviment manual
        if (Input.GetAxis("Horizontal") > 0) this.gameObject.GetComponent<SpriteRenderer>().flipX = false;
        else if (Input.GetAxis("Horizontal") < 0) this.gameObject.GetComponent<SpriteRenderer>().flipX = true;
    }







    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 6) // 6 = Fireball Layer
        {
            PlayerLife--;
        }
    }

    void ControlLife()
    {
        if (PlayerLife <= 0)
        {
            SceneManager.LoadScene("GameOverScreen");
        }
    }
}

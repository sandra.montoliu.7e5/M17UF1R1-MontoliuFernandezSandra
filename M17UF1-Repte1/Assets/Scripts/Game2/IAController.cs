using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IAController : MonoBehaviour
{
    public float PlayerLife;

    private bool irVolver = true;
    public float Speed;
    public float Distance;
    private float _timePatrulla = 1.0f;
    private float _counter = 0;

    // Start is called before the first frame update
    void Start()
    {
        PlayerLife = 1.0f;
        Distance = 6.0f;
        Speed = 1.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerLife <= 0) SceneManager.LoadScene("GameOverScreen");

        Patrulla();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 6) // 6 = Fireball Layer
        {
            PlayerLife--;
        }
    }

    void ControlLife()
    {
        if (PlayerLife <= 0)
        {
            SceneManager.LoadScene("GameOverScreen");
        }
    }

    void Patrulla()
    {
        if (irVolver == true)
        {
            this.gameObject.GetComponent<SpriteRenderer>().flipX = true;
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(Distance, transform.position.y, 0), Speed * Time.deltaTime);

            if (transform.position.x >= Distance)
            {
                if (_counter < _timePatrulla)
                {
                    _counter += Time.deltaTime;
                    // Torna a la posici� inicial
                    //GameObject.Find("Female_01").GetComponent<SpriteRenderer>().sprite = Sprite[1];
                }
                else
                {
                    irVolver = false;
                    _counter = 0;
                }

            }
        }
        else if (irVolver == false)
        {
            this.gameObject.GetComponent<SpriteRenderer>().flipX = false;
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(-Distance, transform.position.y, 0), Speed * Time.deltaTime);

            if (transform.position.x <= -Distance)
            {
                if (_counter < _timePatrulla)
                {
                    _counter += Time.deltaTime;
                    // Torna a la posici� inicial
                    //GameObject.Find("Female_01").GetComponent<SpriteRenderer>().sprite = Sprite[1];
                }
                else
                {
                    irVolver = true;
                    _counter = 0;
                }
            }
        }
    }
}
